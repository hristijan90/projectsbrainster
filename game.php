<?php
require 'data/games.php'; 

if(isset($_GET['gameIndex']) && isset($games[$_GET['gameIndex']])) {
    $gameIndex = $_GET['gameIndex'];
    $game = $games[$gameIndex];
} else {
    header("Location: index.php");
    die();
}
?>

<?php include 'layout/header.php'; ?>

    <img src="<?=$game['img']?>" class="bg-image" width="100%" height="700px" alt="background_image">

    <div class="col-md-10 col-md-offset-1 single-page">
        <h2><b><?=$game['title']?></b></h2>
        <div class="well">
            <p><i class="far fa-clock"></i><b> Временска рамка</b><br><small><?=$game['time']?></small></p>
            <p><i class="fas fa-users"></i><b> Големина на група</b><br><small><?=$game['group']?></small></p>
            <p><i class="fas fa-university"></i><b> Ниво на фасилитирање</b><br><small><?=$game['facility']?></small></p>
            <p><i class="fas fa-paint-roller"></i><b> Материјали</b><br><small><?=$game['materials']?></small></p>
        </div>
        <p><?=$game['text']?></p>
        
        <a href='index.php' class='btn btn-primary back-button' role='button'>Назад</a>
    </div>

<?php include 'layout/footer.php'; ?>