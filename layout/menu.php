<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="menu">
      <div class="col-md-5 col-sm-2 menu-button">
        <ul class="nav navbar-nav navbar-left">
          <li><a type="button" class="btn top-button" id="showSidebar" data-toggle="modal" data-target="#sidebar-hack" data-backdrop="static" data-keyboard="false"><i class="fas fa-bars"></i> Мени</a></li>
        </ul>
      </div>
      <div class="col-md-2 col-sm-2 text-center navbar-logo">
        
          <a class="navbar-brand" href="#"><img src="imgs/logo.png" alt="logo" height="170%"></a>
        
      </div>
      <div class="col-md-5 col-sm-8">
        <ul class="nav navbar-nav navbar-right">
          <li><a class="btn btn-primary top-button" href="https://www.brainster.io/business" id="train-button" target="_blank">Обуки за компании</a></li>
          <!-- id="showFormModal" data-toggle="modal" data-target="#contactModal -->
          <li><a class="btn btn-primary top-button" href="https://www.brainster.io/business" id="employ-button" target="_blank">Вработи наши студенти</a></li>
        </ul>
      </div>
    </div>
  </div>
</nav>