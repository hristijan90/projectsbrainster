

<?php include 'sidebar.php'; ?>
<?php include 'components/register-modal.php'; ?>
<?php include 'components/login-modal.php'; ?>


<div id="bg-up"></div>
<div id="bg-down"></div>

    <div class="container">
        <div class="jumbotron text-center">
            <h1>FUTURE-PROOF YOUR ORGANIZATION</h1>
            <p>Find out how to unlock progress in your business. Understand your current state, identify opportunity areas and prepare to take charge of your organization's future.</p>
            <p><a class="btn btn-primary btn-lg assess-button" href="https://brainsterquiz.typeform.com/to/kC2I9E" target="_blank" role="button">Take the assessment</a></p>
        </div>
    </div>
    
<footer>
    <div class="col-md-5 col-xs-12 first">
        <ul>
            <li><a href="https://brainster.co/about" target="_blank">About us</a></li>
            <li><a href="https://brainster.co/contact" target="_blank">Contact</a></li>
            <li><a href="https://www.facebook.com/brainster.co/photos/" target="_blank">Gallery</a></li>
        </ul>
    </div>
    <div class="col-md-2 col-xs-12 second">
        <a href="#"><img src="imgs/logo.png" alt="logo" width="50%"></a>
    </div>
    <div class="col-md-5 col-xs-12 third">
        <ul>
            <li><a href="https://www.linkedin.com/school/brainster-co/" target="_blank"><i class="fab fa-linkedin-in" style="font-size:27px"></i></a></li>
            <li><a href="https://twitter.com/BrainsterCo" target="_blank"><i class="fab fa-twitter" style="font-size:27px"></i></a></li>
            <li><a href="https://www.facebook.com/brainster.co" target="_blank"><i class="fab fa-facebook-f" style="font-size:27px"></i></a></li>
        </ul>
    <div>
</footer>

<script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>