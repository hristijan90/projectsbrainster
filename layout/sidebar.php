<div id="sidebar-hack" class="modal fade sidebar" role="dialog">
    <div class="content">
        <div class="flex-menu">
            <img src="imgs/logo.png" alt="logo" width="30%">
            <button type="button" class="btn close-button" data-dismiss="modal"><span aria-hidden="true">&times;</span> Затвори</button>
        </div>
        <ul class="list-group">
            <li class="list-group-item"><a href="" id="register" data-toggle="modal" data-target="#registerModal" data-backdrop="static" data-keyboard="false" class="yellow">Регистрирај се</a></li>
            <li class="list-group-item"><a href="" id="login" data-toggle="modal" data-target="#loginModal" data-backdrop="static" data-keyboard="false" class="yellow">Најави се</a></li>
            <li class="list-group-item"><a href="https://brainster.co/about" target="_blank" class="black">За нас</a></li>
            <li class="list-group-item"><a href="https://www.facebook.com/pg/brainster.co/photos/" target="_blank" class="black">Галерија</a></li>
            <li class="list-group-item"><a href="https://brainster.co/contact" target="_blank" class="black">Контакт</a></li>
        </ul>
    </div>
</div>