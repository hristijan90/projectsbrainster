<?php
$msgs = [
    'registered' => "Registration successful. Now please log in.",
    'nameRequired' => "Registration unsuccessful. First name is required",
    'lastNameRequired' => "Registration unsuccessful. Last name is required",
    'companyRequired' => "Registration unsuccessful. Company name is required",
    'emailRequired' => "Registration unsuccessful. Email adress is required",
    'enterCompany' => "Login unsuccessful. Enter your company name",
    'enterEmail' => "Login unsuccessful. Enter your email adress",
    'companyNotFound' => "Login unsuccessful. Company name not found",
    'emailIncorrect' => "Login unsuccessful. Incorrect email adress",
    'companyExists' => "Company is already registered",
    'emailExists' => "The email adress is already registered"
    ];
?>