<?php require 'data/games.php'; ?>
<?php include 'data/messages.php'; ?>
<?php include 'layout/header.php'; ?>
<?php session_start(); ?>
    
    <div class="container">

        <h5 class="text-center white createdBy">Изработено од студенти на академијата за програмирање на <a href="https://brainster.co/" target="_blank" class="brainsterLink">Brainster</a><h5>

        <?php            
            foreach($msgs as $key => $value) {
                if(isset($_GET[$key])) {
                    echo "<p class='text-center welcome'>$value</p>";
                }
            }
            if(isset($_SESSION['firstName'])) {
                $name = $_SESSION['firstName']." ".$_SESSION['lastName'];
                $company = $_SESSION['company'];
                echo "<p class='text-center welcome'>Welcome $name from $company. <a href='components/logout.php' class='yellow'><u>
                 Logout here.</u></a></p>";
            }
        ?>

        <div class="jumbotron text-center">
            <h1>FUTURE-PROOF YOUR ORGANIZATION</h1>
            <p>Find out how to unlock progress in your business. Understand your current state, identify opportunity areas and prepare to take charge of your organization's future.</p>
            <p><a class="btn btn-primary btn-lg assess-button" href="https://brainsterquiz.typeform.com/to/kC2I9E" target="_blank" role="button">Take the assessment</a></p>
        </div>
        
        <?php
        foreach ($games as $key => $game) {
            
            $img = $game['img'];
            $title = $game['title'];
            $icon = $game['icon'];
            $category = $game['category'];
            $time = $game['time'];
            
            echo "<div class='col-xs-12 col-sm-6 col-md-4'>";
            echo "<a href='game.php?gameIndex=$key' class='linkGame' role='button'>";
            echo "<div class='thumbnail'>";
            echo "<img src='$img' alt='Game image' id='cardImage'>";
            echo "<div class='caption'>";
            echo "<img src='$icon' class='smallIcon img-circle'>";
            echo "<h4><b>$title</b></h4>";
            echo "<p>Категорија: <span class='category'>$category</span></p><br>";
            echo "<p><span class='glyphicon glyphicon-time' aria-hidden='true'></span> <b>Времетраење</b><br><small>$time</small></p>";
            echo "</div>";
            echo "</div>";
            echo "</a>";
            echo "</div>";

        }
        ?>

    </div>

<?php include 'layout/footer.php'; ?>