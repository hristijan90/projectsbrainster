<?php
if($_SERVER['REQUEST_METHOD'] == 'POST') {

    if(empty($_POST['firstName']) == false) {
        $firstName = $_POST['firstName'];
    } else {
        header('Location: ../index.php?nameRequired');
        die();
    } 
    if(empty($_POST['lastName']) == false) {
        $lastName = $_POST['lastName'];
    } else {
        header('Location: ../index.php?lastNameRequired');
        die();
    }
    if(empty($_POST['company']) == false) {
        $company = $_POST['company'];
    } else {
        header('Location: ../index.php?companyRequired');
        die();
    } 
    if(empty($_POST['email']) == false) {
        $email = $_POST['email']; 
    } else {
        header('Location: ../index.php?emailRequired');
        die();
    }
    $phoneNumber = $_POST['phoneNumber'];
    $numOfEmployees = $_POST['numOfEmployees'];
    $department = $_POST['department'];
    $message = $_POST['message'];

    $username = "root";
    $password = "rootroot";

    try {
        $dbh = new PDO('mysql:host=localhost;dbname=project1', $username, $password,  
            array(PDO::ATTR_PERSISTENT => true));
    } catch (Exception $e) {
        die("Unable to connect: " . $e->getMessage());
    }

    $sthandler = $dbh->prepare("SELECT company FROM info WHERE company = :company");
    $sthandler->bindParam(':company', $company);
    $sthandler->execute();

    if($sthandler->rowCount() > 0){
        header('Location: ../index.php?companyExists');
        die();
    }
    $sthandler = $dbh->prepare("SELECT email FROM info WHERE email = :email");
    $sthandler->bindParam(':email', $email);
    $sthandler->execute();

    if($sthandler->rowCount() > 0){
        header('Location: ../index.php?emailExists');
        die();
    }
      
    try {  
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        $dbh->beginTransaction();
        $dbh->exec("INSERT INTO info (`firstName`, `lastName`, company, email, phoneNumber, numOfEmployees, department, `message`) VALUES ('$firstName', '$lastName', '$company', '$email', '$phoneNumber', '$numOfEmployees', '$department', '$message')");
        $dbh->commit();
        header('Location: ../index.php?registered');
    
    } catch (Exception $e) {
        $dbh->rollBack();
        echo "Failed: " . $e->getMessage();
    }
}


?>