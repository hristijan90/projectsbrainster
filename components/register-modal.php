<div id="registerModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form method="POST" action="components/register.php">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Register</b></h4>
                </div>
                <div class="modal-body">               
                    
                    <div class="form-group">
                        <input type="text" name="firstName" class="form-control" placeholder="First Name">
                    </div>
                    <div class="form-group">
                        <input type="text" name="lastName" class="form-control" placeholder="Last Name">
                    </div>
                    <div class="form-group">
                        <input type="text" name="company" class="form-control" placeholder="Company">
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="text" name="phoneNumber" class="form-control" placeholder="Phone Number (Optional)">
                    </div>
                    <div class="form-group">
                        <label for="numOfEmployees">Number of Employees</label>
                        <select name="numOfEmployees" id="numOfEmployees" class="form-control">
                            <option value="1">1</option>
                            <option value="2-10">2-10</option>
                            <option value="11-50">11-50</option>
                            <option value="51-200">51-200</option>
                            <option value="200+">200+</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="department">Department</label>
                        <select name="department" id="department" class="form-control">
                            <option value="Human Resources">Human Resources</option>
                            <option value="Marketing">Marketing</option>
                            <option value="Product">Product</option>
                            <option value="Sales">Sales</option>
                            <option value="CEO">CEO</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <textarea type="text" name="message" class="form-control" maxlength="300" placeholder="Please use this space to tell us more about your needs. Our team can craft custom solutions around reskilling, upskilling, onboarding, hiring and benchmarking our talent." rows="5" oninput="enableBtn7();"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn modal-button">Register</button>
                </div>
            </div>
        </form>
    </div>
</div>