<?php
if($_SERVER['REQUEST_METHOD'] == 'POST') {
    session_start();
    ob_start();

    if(empty($_POST['company']) == false) {
        $company = $_POST['company'];
    } else {
        header('Location: ../index.php?enterCompany');
        die();
    } 
    if(empty($_POST['email']) == false) {
        $email = $_POST['email']; 
    } else {
        header('Location: ../index.php?enterEmail');
        die();
    }

    $username = "root";
    $password = "rootroot";

    try {
        $connect = new PDO('mysql:host=localhost;dbname=project1', $username, $password,  
            array(PDO::ATTR_PERSISTENT => true));
    } catch (Exception $e) {
        die("Unable to connect: " . $e->getMessage());
    }

    try {
        $stmt = $connect->prepare('SELECT firstName, lastName, company, email FROM info WHERE company = :company');
        $stmt->execute(array(':company' => $company));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        if($data == false){
            header('Location: ../index.php?companyNotFound');
            die();
        } else {
            //vo proektot nema password zatoa proveruvam company name i email
            if($email == $data['email']) {
                $_SESSION['firstName'] = $data['firstName'];
                $_SESSION['lastName'] = $data['lastName'];
                $_SESSION['company'] = $data['company'];
                
                header('Location: ../index.php');
                exit;
            } else {
                header('Location: ../index.php?emailIncorrect');
                die();
            }
        }
    }

    catch(PDOException $e) {
        $errMsg = $e->getMessage();
    }

}
?>