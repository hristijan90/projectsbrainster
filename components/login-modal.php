<div id="loginModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form method="POST" action="components/login.php">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Login</b></h4>
                </div>
                <div class="modal-body">               
                    
                    <div class="form-group">
                        <label for="company">Company</label>
                        <input type="text" name="company" id="company" class="form-control" placeholder="Enter your company name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" class="form-control" placeholder="Enter your company email adress">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn modal-button">Login</button>
                </div>
            </div>
        </form>
    </div>
</div>